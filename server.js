// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = 8080;

var tasks = {
    333: {
        name: "first",
        description: "descr",
        id: "333"
    }
};

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function (req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function (req, res) {
    res.json({message: 'hooray! welcome to our api!'});
});

router.route('/tasks')
// create a task (accessed at POST http://localhost:8080/api/tasks)
    .post(function (req, res) {

        var task = {
            name: req.body.name,
            description: req.body.description,
            id: req.body.id
        };
        tasks[task.id] = task;

        res.json({message: 'Task created!'});
        console.log('TASKS :', tasks);

    })
    .get(function (req, res) {
        res.json(tasks);
    });


router.route('/tasks/:task_id')

// get the bear with that id (accessed at GET http://localhost:8080/api/bears/:bear_id)
    .get(function (req, res) {
        var task = tasks[req.params.task_id];
        if (task) {
            res.json(task)
        } else {
            res.json({message: 'Task not finded!'});
        }


    })
    .put(function (req, res) {

        // use our bear model to find the bear we want
        var task = tasks[req.params.task_id];
        if (task) {
            if (req.body.name) {
                task.name = req.body.name
            }
            if (req.body.description) {
                task.description = req.body.description
            }
            res.json({
                message: 'Task updated!',
                name: req.body.name,
                description: req.body.description
            });
        } else {
            res.json({message: 'Task not found!'});
        }


    })
    .delete(function (req, res) {
        var task = tasks[req.params.task_id];
        if (task) {
            delete tasks[req.params.task_id];
            res.json({
                message: 'Task deleted!'
            });
        } else {
            res.json({message: 'Task not found!'});
        }
    });


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);


